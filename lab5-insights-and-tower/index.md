# Lab 5: Automatic Remediation with Red Hat Insights and Ansible Tower

<!-- TOC -->

- [Lab 5: Automatic Remediation with Red Hat Insights and Ansible Tower](#lab-5-automatic-remediation-with-red-hat-insights-and-ansible-tower)
  - [Goal of Lab](#goal-of-lab)
  - [Introduction](#introduction)
    - [Setting up an Insights Scan Project](#setting-up-an-insights-scan-project)
    - [Create Insights Credentials](#create-insights-credentials)
    - [Creating an inventory](#creating-an-inventory)
    - [Creating a Scan Project](#creating-a-scan-project)
    - [Creating a Job Template](#creating-a-job-template)
    - [Viewing Insights data into Tower](#viewing-insights-data-into-tower)
- [BONUS!](#bonus)
  - [Automatically remediate Insights Inventory](#automatically-remediate-insights-inventory)
    - [Creating a Remediation Project](#creating-a-remediation-project)
    - [Creating a Remediation Job Template](#creating-a-remediation-job-template)

<!-- /TOC -->

## Goal of Lab

The goal of this lab is to introduce you to the proactive security
capabilities of Red Hat Insights and automatic remediation with Ansible
Tower. This lab assumes that you started all your VMs, as instructed in
Lab 0, which is necessary for Red Hat Insights to work properly.  You
should require no prior Tower knowledge to follow all instructions.

## Introduction

Red Hat Insights is designed to proactively evaluate the security,
performance, and stability of your Red Hat platforms by providing
prescriptive analytics of your systems. Red Hat Insights helps move you
from reactive to proactive systems management, delivers actionable
intelligence, and increases visibility of infrastructure risks and the
latest security threats. Operational analytics from Red Hat Insights
empowers you to minimize downtime and firefighting while responding
faster to new risks.

### Setting up an Insights Scan Project

Tower supports integration with Red Hat Insights. Once a host is
registered with Insights, it will be continually scanned for
vulnerabilities and known configuration conflicts. Each of the found
problems may have an associated fix in the form of an Ansible playbook.
Insights users create a maintenance plan to group the fixes and,
ultimately, create a playbook to mitigate the problems.

Tower tracks the maintenance plan playbooks via an Insights project in
Tower. Authentication to Insights via Basic Auth, from Tower, is
backed by a special Insights Credential, which must first be
established in Tower. To run an Insights Maintenance Plan in Tower,
you need an Insights project, an inventory, and a Scan Job template.
Credentials have already been provided for your convenience.

If you are continuing from the previous lab you may need to log out of the "arocks" user.
Log into Tower using the "admin" user with the password "r3dh4t1!"

### Creating an Inventory

The Insights playbook contains a `hosts:` line where the value is the
hostname that Insights itself knows about, which may be different than
the hostname that Tower knows about. Therefore, make sure that the
hostnames in the Tower inventory match up with the system in the Red Hat
Insights Portal.

For your convenience, the Insights inventory has been already created
for you.

![](images/image126.png)

Please note that typically, your inventory already contains Insights
hosts. Tower just doesn’t know about them yet. The Insights credential
allows Tower to get information from Insights about an Insights host.
Tower can identify a host as an Insights host without Insights 
credentials by doing an Ansible facts scan.

In order for Tower to utilize Insights Maintenance Plans, it must have
awareness of them. Create and run a scan job against the inventory
using a stock manual scan playbook (this is provided by Red Hat to its
customers).

### Creating a Scan Project

1.  Click the **Projects** main link to access the Projects page.
2.  Click the ![Add](images/image23.png) button, which launches the New
    Project window.
3.  Enter the appropriate details into the required fields, at minimum.
    Note the following fields requiring specific Insights-related
    entries:
    -   Name: Insights Scan Summit
    -   Organization: Default
    -   SCM Type: Git
    -   Upon selecting the SCM type, the Source Details field expands.
4. In the SCM URL field, enter
https://github.com/ansible/awx-facts-playbooks. This is the location
where the scan job template is stored.
5. Click Save when done.

Your screen should look as the following:

![](images/image86.png)

All SCM/Project syncs occur automatically the first time you save a new
project. However, if you want them to be updated to what is current in
Insights, manually update the SCM-based project by clicking the
![update](images/image101.png) button under the project’s available
Actions.

Syncing imports into Tower any Maintenance Plans in your Insights
account that has a playbook solution. It will use the default Plan
resolution. Notice that the status dot beside the name of the project
updates once the sync has run.

Now it’s the time to put all the pieces together, by using a job
template that uses the fact scan playbook.

### Creating a Job Template

1.  Click the **Templates** main link to access the Templates page.
2.  Click the ![add](images/image23.png) button and select Job Template,
    which launches the New Job Template window.
3.  Enter the appropriate details into the required fields, at minimum.
    Note the following fields requiring specific Insights-related
    entries:

-   Name: Insights Scan Summit
-   Job Type: Run
-   Inventory: Example.com Clients
-   Project: Insights Scan Summit
-   Playbook: scan_facts.yml
-   Credential: Example.com SSH Password
-   Click to select Use Fact Cache from the Options field.

4.  Click Save when done.

Your screen should look as follows:

![](images/image33.png)

Click the ![launch](images/image71.png) icon to launch the scan job
template, the output you should see is something like this, if it all
went as expected:

![](images/image24.png)

With this, we know the state our machines are in and issues they may
have that should be remediated. This is what we are going to do in the
next part of the lab.

Now, we can see Insights data from the Ansible Tower UI.

### Viewing Insights data into Tower

1.  Click the **Inventories** link to access the Inventories page.
2.  In the list of inventories, click to open the details of your
    Example.com Clients inventory.

![](images/image122.png)

3.  Click the Hosts tab to access the Insights hosts that have been
    loaded from the scan process.
4.  Click to open one of the hosts that was loaded from Insights
    (ic1.example.com, for instance).

![](images/image97.png)

Notice the Insights tab is now shown on Hosts page. This indicates that
Insights and Tower have reconciled the inventories and is now set up for
one-click Insights playbook runs. Click on it.

![](images/image119.png)

You now can see a list of issues Insights has identified and whether or
not the issues can be resolved with a playbook is also shown.

# BONUS!


## Automatically remediate Insights Inventory


### Creating a Remediation Project

Remediation of an Insights inventory allows Tower to run Insights
playbooks with a single click, instead of using the CLI as we did in the
previous Lab.

First thing is to create a remediation project, similar to the scan
project previously created.

1.  Click the **Projects main** link to access the Projects page.
2.  Click the ![add](images/image23.png) button, which launches the New
    Project window.

3.  Enter the appropriate details into the required fields, at minimum.
    Note the following fields requiring specific Insights-related
    entries:

-   Name: Insights Remediation Summit
-   Organization: Default, or click the
    ![search](images/image121.png) button and select it from the pop-up
    window.
-   SCM Type: Git
-   SCM URL: https://github.com/chris-short/red-hat-summit-2019
-   SCM UPDATE OPTIONS: Tick all three boxes to always get a fresh copy
    of the repository and to update the repository when launching a job

4.  Click on Save

Your screen should look as follows:

![](images/image150.png)

NOTE: In the real world, we would be accessing directly to our Red Hat
account, however for the limitations of the configuration in this lab,
this is not possible. In the real world, we’d need to choose Red Hat
Insights as the SCM type and our Customer Portal Credentials (later in
this lab we’d need to use them, and they have already been created for
your convenience).

Insights as the SCM type and our Customer Portal Credentials.
screen:

![](images/image146.png)

### Creating a Remediation Job Template

Similar to the one created before, we are to create a remediation job
template that can use the remediation project we have just created.

4.  Click the **Templates main** link to access the Projects page.
5.  Click the ![add](images/image23.png) button and select Job Template,
    which launches the New Job Template window.
6.  Enter the appropriate details into the required fields, at minimum.
    Note the following fields requiring specific Insights-related
    entries:

-   Name: Insights Remediation Summit
-   Job Type: Run
-   Inventory: Example.com Clients
-   Project: Insights Remediation Summit
-   Playbook: Select 41857.yml from the drop-down menu list. This is
    the playbook associated with the Remediation project you previously
    set up.
-   Credential: Example.com SSH Password. The credential does not have
    to be an Insights credential, but machine (also created for your
    convenience).

7.  Click Save when done.

Your screen should look as follows:

![](images/image170.png)

Now, simply execute it by clicking on the rocket next to the name of the
template in the templates list down the screen.

![](images/image187.png)

The playbook execution screen appears and shows you the result.

![](images/image1123.png)

**Note:** There is a known race condition where when executing the playbook
it will fail on the first run for some older RHEL 7.0 hosts. Simply
re-run the playbook and everything should complete successfully, and the
hosts that failed the first time will complete
remediation.

Continue to the next step: [Lab 6: Build a Service Catalog with CloudForms](../lab6-self-service-portal-with-cloudforms/index.md)
